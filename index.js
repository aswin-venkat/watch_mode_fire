var mysql      = require('mysql');
var admin = require("firebase-admin");
var connection = mysql.createConnection({ host: "35.200.241.216",
                                          user: "root",
                                          password: "vtozone",
                                          database: "vt_primary"
                                        });

connection.connect();  

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://vehicletracker360.firebaseio.com"
});

var db = admin.database(); 
var dbFire = admin.firestore();                                        
 
connection.query('SELECT * FROM `watch_mode`', function (error, result) {
  if (error) throw error;

  for(let i=0; i<result.length; i++) {
    console.log('IMEI: ', result[i].imei, 'Status: ', result[i].status, 'Lat: ', result[i].lat, 'Lng: ', result[i].lng, 'SMS: ', result[i].sms, 'Count: ', result[i].count, 'Notification: ', result[i].notification_snooze);

    db.ref('objects_stream').orderByChild('imei').equalTo(result[i].imei).on('child_added', function(snap) {
      console.log('UID is: ', snap.val().uid);
      dbFire.collection('personal_surveillance').doc(snap.val().uid).update({
        current_state: result[i].status,
        coordinates: {lat: result[i].lat, lng: result[i].lng},
        traces: {sms_cond: result[i].sms, notification_cond: result[i].notification_snooze, usage_count: result[i].count}
      });
    });
  }
});

